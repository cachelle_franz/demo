package Steps;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class CucumberRunner {


    public static WebDriver obj,obj1 ;

    @Given("^i navigate to login page$")
    public void iNavigateToLoginPage() throws Throwable {


        System.setProperty("webdriver.chrome.driver", "E:\\installers\\chromedriver_win32\\chromedriver.exe");
        obj = new ChromeDriver();
        obj.get("http://localhost/fligno/");


    }
    @And("^i enter the username and password$")
    public void iEnterTheUsernameAndPassword(DataTable table) throws Throwable {

        obj.findElement(By.xpath("//*[@id=\"username\"]")).sendKeys("admin");
        obj.findElement(By.xpath("//*[@id=\"password\"]")).sendKeys("admin");

        obj.manage().timeouts().implicitlyWait(10000, TimeUnit.SECONDS);
        Thread.sleep(2000);
    }

    @And("^i click login button$")
    public void iClickLoginButton() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        obj.findElement(By.xpath("//*[@id=\"login\"]/form/div/div/div/div[3]/div[1]/button")).click();

    }

    @Then("^direct to homepage$")
    public void directToHomepage() throws Throwable {
        if (obj.getCurrentUrl().equalsIgnoreCase("http://[::1]/fligno/index.php/login/user_login_process")){

            System.out.println("passed");
        }
        else {
            System.out.println("Test2 Failed");
        }
        Thread.sleep(2000);
        obj.close();

    }



    @And("^I enter wrong username and password$")
    public void iEnterWrongUsernameAndPassword() throws Throwable {
        obj.findElement(By.xpath("//*[@id=\"username\"]")).sendKeys("wrong");
        obj.findElement(By.xpath("//*[@id=\"password\"]")).sendKeys("admin");

        Thread.sleep(2000);
    }

    @Then("^print error$")
    public void printError() throws Throwable {
        if (obj.getCurrentUrl().equalsIgnoreCase("http://localhost/fligno/index.php/login/user_login_process")){

            System.out.println("passed");
        }
        else {
            System.out.println("Test2 Failed");
        }
        obj.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        obj.close();

    }

    @Then("^direct to admin page$")
    public void directToAdminPage() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        if (obj.getCurrentUrl().equalsIgnoreCase("http://localhost/fligno/index.php/login/user_login_process")){

            System.out.println("passed");
        }
        else {
            System.out.println("Test2 Failed");
        }
        obj.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        Thread.sleep(2000);
    }


    @And("^I click add button$")
    public void iClickAddButton() throws Throwable {
        obj.findElement(By.xpath("//*[@id=\"add\"]")).click();
        Thread.sleep(2000);
    }

    @And("^I type new user$")
    public void iTypeNewUser() throws Throwable {
        obj.findElement(By.xpath("//*[@id=\"Region\"]")).sendKeys("new");
        obj.findElement(By.xpath("//*[@id=\"Contact_no\"]")).sendKeys("new");
        Thread.sleep(2000);
    }

    @When("^I cllick submit button$")
    public void iCllickSubmitButton() throws Throwable {
        obj.findElement(By.xpath("//*[@id=\"myModal\"]/div/div/div[3]/div/button")).click();
        Thread.sleep(2000);
    }



    @Then("^I click edit$")
    public void iClickEdit() throws Throwable {

        obj.findElement(By.xpath("//*[@id=\"example1\"]/tbody/tr[1]/td[3]/a[1]")).click();
        Thread.sleep(2000);
    }

    @Then("^I should redirect to edit form$")
    public void iShouldRedirectToEditForm() throws Throwable {
        Thread.sleep(2000);
    }

    @And("^I click edit update button$")
    public void iClickEditUpdateButton() throws Throwable {

        obj.findElement(By.xpath("//*[@id=\"update\"]/div/div/div[3]/div/button")).click();
        Thread.sleep(2000);
    }

    @Then("^I edit fields$")
    public void iEditFields() throws Throwable {
        obj.findElement(By.xpath("//*[@id=\"usrname\"]")).sendKeys("update");
        obj.findElement(By.xpath("//*[@id=\"position\"]")).sendKeys("update");
        Thread.sleep(2000);
    }

    @Given("^i navigate to home page$")
    public void iNavigateToHomePage() throws Throwable {
        System.setProperty("webdriver.chrome.driver", "E:\\installers\\chromedriver_win32\\chromedriver.exe");
        obj1 = new ChromeDriver();
        Thread.sleep(2000);
        obj1.get("http://[::1]/fligno/index.php/login/user_login_process");
    }

    @And("^i click logout button$")
    public void iClickLogoutButton() throws Throwable {
        obj.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        obj.findElement(By.xpath("/html/body/header/nav/div/ul/li[1]/a")).click();
        obj.findElement(By.xpath("//*[@id=\"logout\"]/a")).click();

    }

    @Then("^I should redirect to logout form$")
    public void iShouldRedirectToLogoutForm() throws Throwable {
        if (obj.getCurrentUrl().equalsIgnoreCase("http://[::1]/fligno/index.php/login/logout")){

            System.out.println("logout test passed");
        }
        else {
            System.out.println("Test2 Failed");
        }

        obj.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
       if( obj.getPageSource().contains("Successfully Logout")){
           System.out.println("logout test passed2");
       }
       else {
           System.out.println("failed");
       }
        Thread.sleep(2000);
       obj.close();
    }


    @Then("^I redirect to homepage$")
    public void iRedirectToHomepage() throws Throwable {
        if (obj.getCurrentUrl().equalsIgnoreCase("http://localhost/fligno/index.php/login/user_login_process")){
            System.out.println("Login passed");
        }
        else {
            System.out.println("Test2 Failed");
        }
        Thread.sleep(2000);
    }
}
