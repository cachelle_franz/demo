Feature: Login
  As a [role]
  I want [feature]
  So that [benefit/business reason]

  Scenario: Login with correct username and password
    Given i navigate to login page
    And i enter the username and password
      | username | password |
      | admin    | admin    |
    And i click login button
    Then direct to homepage


    Scenario: Login with wrong credential
      Given i navigate to login page
      And I enter wrong username and password
      And i click login button
      Then print error


      Scenario: Logout
        Given i navigate to login page
        And i enter the username and password
          | username | password |
          | admin    | admin    |
        And i click login button
        Then I redirect to homepage
        And i click logout button
        Then I should redirect to logout form

