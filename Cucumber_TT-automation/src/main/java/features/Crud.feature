Feature: Add new Item

  Scenario: I add new item
    Given i navigate to login page
    And i enter the username and password
      | username | password |
      | admin    | admin    |
    And i click login button
    Then direct to admin page
    And I click add button
    And I type new user
    When I cllick submit button

    Scenario: I can edit an item
      Given i navigate to login page
      And i enter the username and password
        | username | password |
        | admin    | admin    |
      And i click login button
      Then direct to admin page
      Then I click edit
      Then I should redirect to edit form
      Then I edit fields
      And I click edit update button


