-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 02, 2019 at 01:54 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dir`
--

-- --------------------------------------------------------

--
-- Table structure for table `membership`
--

CREATE TABLE `membership` (
  `membership_id` int(11) NOT NULL,
  `membership_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sectors`
--

CREATE TABLE `sectors` (
  `sector_id` int(11) NOT NULL,
  `setor_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sectors`
--

INSERT INTO `sectors` (`sector_id`, `setor_name`) VALUES
(1, 'nro');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `user_type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `password`, `user_type`) VALUES
(1, 'admin', 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `user_id` int(11) NOT NULL,
  `username` varchar(30) DEFAULT NULL,
  `sector_id` int(11) NOT NULL,
  `membership_id` int(11) DEFAULT NULL,
  `office` varchar(50) DEFAULT NULL,
  `tel_no` varchar(50) DEFAULT NULL,
  `fax_no` varchar(50) DEFAULT NULL,
  `position` varchar(50) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `website` varchar(50) DEFAULT NULL,
  `other_info` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`user_id`, `username`, `sector_id`, `membership_id`, `office`, `tel_no`, `fax_no`, `position`, `address`, `email`, `website`, `other_info`) VALUES
(17, 'sample', 0, 0, '11', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(18, 'sample', 0, 0, '11', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(19, 'sample', 0, 0, '11', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(20, 'sample', 0, 0, '11', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(21, 'sample', 2, 0, '11', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(22, 'sample', 2, 0, '11', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(23, 'sample', 2, 0, '11', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(24, 'sample', 3, 3, '11', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(25, 'sample', 3, 3, '11', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(26, 'sample', 3, 2, '11', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(29, 'sample', 5, 5, '11', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'new', 'nre'),
(30, 'sample', 1, 7, 'dsfdsf', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(31, 'bubbles', 1, 7, '1112', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(32, 'sample', 1, 7, '1121212', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(33, 'sample', 1, 7, 'jbvdfdfg dfg', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'new', NULL),
(34, 'sample', 1, 7, '11', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', '.com', NULL),
(35, 'sample', 1, 7, '11', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(36, 'sample', 3, 0, '11', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(37, 'sample', 1, 6, '111212', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(38, 'sample', 1, 6, '11', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'dsd', 'dsdsd'),
(49, 'sample23232323', 2, 1, NULL, NULL, NULL, 'cachelleat17@gmail.comgfgfg', NULL, NULL, NULL, NULL),
(51, 'sample', 3, 2, '11', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(52, 'sample', 3, 2, '11', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'yes', 'busy'),
(53, 'sample', 3, 2, '11', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(54, 'sample', 3, 2, '11', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'leee'),
(55, 'sample', 3, 2, '11', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(56, 'sample', 3, 3, '11', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(60, 'sample', 2, 1, '11', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'sds', 'sdsd'),
(62, 'sample', 5, 2, '1112', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'leee'),
(65, 'sample', 5, 2, 'wqwqw', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'this'),
(66, 'sample', 5, 5, '11', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'ria', 'nfdf'),
(67, 'sample', 5, 5, '11', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(68, 'sample', 5, 5, '11', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(126, 'sample', 0, 0, '11', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(127, 'sample', 0, 0, '11', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(128, 'sample', 0, 0, '11', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(129, 'sample', 0, 0, '11', 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(134, 'new', 6, 0, 'newewe', 'new', 'new', 'new', 'new', 'new@gmail.com', 'cachelleat17@gmail.com', 'this'),
(138, 'one', 6, 0, 'one', 'one', 'one', 'one', 'one', 'cachelleat17@gmail.com', NULL, NULL),
(139, 'dsd', 6, 0, 'dp', '4545', 'sd', 'leeee', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'rtrt'),
(140, 'sample', 6, 0, 'up', '4545', 'dfd', 'sample', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'asas'),
(141, NULL, 1, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(142, NULL, 1, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(143, NULL, 1, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(144, NULL, 1, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(145, NULL, 1, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(146, NULL, 1, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(147, NULL, 1, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(148, NULL, 1, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(149, NULL, 1, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(150, NULL, 1, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(151, NULL, 1, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(152, NULL, 1, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(153, NULL, 1, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(154, NULL, 1, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(155, NULL, 1, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(156, NULL, 1, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(157, NULL, 1, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(158, NULL, 1, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(159, NULL, 1, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(160, NULL, 1, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(161, NULL, 1, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(162, NULL, 1, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(163, NULL, 1, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(164, NULL, 1, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(165, 'web', 1, 7, '10', 'web', 'web', 'web', 'wbe', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', '                         Entweer your text here...'),
(166, 'sample', 1, 6, 'sample', 'sample', 'sample', 'sample', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', '                         Enter yosamplur text here...'),
(167, 'sample', 1, 6, 'sample', 'sample', 'sample', 'sample', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', '                         Enter yosamplur text here...'),
(169, 'sample', 2, 2, 'Laguindingan', 'ksdjks', 'kldnks', 'sample', 'sjda', 'cachelleat17@gmail.com', 'rtr', 'rtrt'),
(170, 'sample', 2, 2, 'Laguindingan', 'ksdjks', 'kldnks', 'sample', 'sjda', 'cachelleat17@gmail.com', NULL, NULL),
(171, 'sample', 2, 2, 'Laguindingan', 'ksdjks', 'kldnks', 'sample', 'sjda', 'cachelleat17@gmail.com', NULL, NULL),
(172, 'sample', 2, 2, 'Laguindingan', 'ksdjks', 'kldnks', 'sample', 'sjda', 'cachelleat17@gmail.com', NULL, NULL),
(173, NULL, 2, NULL, NULL, 'ksdjks', 'kldnks', 'sample', 'sjda', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', NULL),
(174, NULL, 2, NULL, NULL, 'ksdjks', 'kldnks', 'sample', 'sjda', 'kkeee@gmail.com', 'cachelleat17@gmail.com', NULL),
(175, NULL, 2, NULL, NULL, 'ksdjks', 'kldnks', 'sample', 'sjda', 'kkeee@gmail.com', 'fdfdgmail.com', NULL),
(176, NULL, 2, NULL, NULL, 'ksdjks', 'kldnks', 'sample', 'sjda', 'cachelleat17@gmail.com', 'rtr', 'rtrt'),
(177, NULL, 2, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'c', 'leee'),
(180, 'new', 1, 2, 'new', 'newn', 'new', 'newn', 'new', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', '                         nre'),
(181, NULL, 1, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'yes', 'busy'),
(185, 'new', 2, 2, 'new', 'new', 'new', 'newn', 'new', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', '                         neww'),
(186, 'bew', 2, 0, 'new', 'new', 'new', 'new', 'new', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', '           bew              '),
(187, 'sample', 2, 0, 'new', '3434', '3434', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', '                         asdas'),
(188, 'sample', 2, 0, 'new', '3434', '3434', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', '                         asdas'),
(189, 'sample', 2, 0, 'new', '3434', '3434', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', '                         asdas'),
(190, 'sample', 2, 0, 'new', '3434', '3434', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', '                         asdas'),
(191, 'new', 2, 0, 'new', 'new', 'newn', 'newn', 'new', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', '                         newnewnew'),
(192, 'new', 2, 0, 'new', 'new', 'newn', 'newn', 'new', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', '                         newnewnew'),
(193, 'new', 2, 0, 'new', 'neww', 'new', 'new', 'new', 'cachelleat17@gmail.com', NULL, NULL),
(194, 'new', 2, 0, 'new', 'neww', 'new', 'new', 'new', 'cachelleat17@gmail.com', NULL, NULL),
(195, 'new', 2, 0, 'new', 'neww', 'new', 'new', 'new', 'cachelleat17@gmail.com', NULL, NULL),
(196, 'sample', 2, 0, '10', '3434', '3434', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', '                         sasas'),
(197, 'sample', 2, 0, '10', '3434', '3434', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', '                         sasas'),
(198, 'g', 2, 0, 'g', 'g', 'g', 'g', 'g', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', '                         g'),
(199, 'g', 2, 0, 'g', 'g', 'g', 'g', 'g', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', '                         g'),
(200, 'n', 2, 0, 'njkd kfbsdkjf', 'n', 'n', 'n', 'n', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', '                         n'),
(201, 'n', 6, 0, 'massdsdsd', 'n', 'n', 'n', 'n', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'n'),
(203, 'new', 1, 3, 'psr', 'mew', 'mew', 'mew', 'mew', 'ina_avery@yahoo.com', 'cachelleat17@gmail.com', '                         wewe'),
(204, 'new', 1, 3, 'psr', 'mew', 'mew', 'mew', 'mew', 'ina_avery@yahoo.com', 'cachelleat17@gmail.com', '                         wewe'),
(205, 'new', 3, 3, 'psr', 'mew', 'mew', 'mew', 'mew', 'ina_avery@yahoo.com', 'cachelleat17@gmail.com', '                         wewe'),
(207, NULL, 5, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'this', 'this'),
(208, 'new', 7, 0, 'new', 'dsd', 'sdsd', 'cachelleat17@gmail.com', 'sample', 'ina_avery@yahoo.com', NULL, NULL),
(209, 'new', 7, 0, 'new', 'dsd', 'sdsd', 'cachelleat17@gmail.com', 'sample', 'ina_avery@yahoo.com', NULL, NULL),
(210, 'must', 7, 0, 'sasas', ',ust', 'mus', 'must', 'must', 'cachelleat17@gmail.com', 'new', 'leee'),
(211, 'must', 7, 0, 'must', ',ust', 'mus', 'must', 'must', 'cachelleat17@gmail.com', NULL, NULL),
(212, 'must', 7, 0, 'must', ',ust', 'mus', 'must', 'must', 'cachelleat17@gmail.com', NULL, NULL),
(214, 'must', 7, 0, 'must', ',ust', 'mus', 'must', 'must', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'leee'),
(215, 'must', 7, 0, 'must', ',ust', 'mus', 'must', 'must', 'cachelleat17@gmail.com', NULL, NULL),
(216, 'must', 7, 0, 'must', ',ust', 'mus', 'must', 'must', 'cachelleat17@gmail.com', NULL, NULL),
(217, 'must', 7, 0, 'must', ',ust', 'mus', 'must', 'must', 'cachelleat17@gmail.com', NULL, NULL),
(218, 'must', 7, 0, 'must', ',ust', 'mus', 'must', 'must', 'cachelleat17@gmail.com', NULL, NULL),
(219, 'must', 7, 0, 'must', ',ust', 'mus', 'must', 'must', 'cachelleat17@gmail.com', NULL, NULL),
(222, 'sample', 7, 0, '20', '3434', '3434', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'rtrt'),
(224, 'sample', 7, 0, '10', '3434', '3434', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(225, 'bne', 7, 0, 'udstp', '3434', '3434', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'rtrt'),
(226, 'c', 7, 0, 'sasas', '3434', '3434', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'rtrt'),
(227, 'cachelleat17@gmail.com', 7, 0, '1112', '3434', '3434', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'rtrt'),
(229, 'new', 6, 0, 'same', '4545', 'sd', 'leeee', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'rtrt'),
(230, 'cachelleat17@gmail.com', 6, 0, 'wqwqw', '4545', 'sd', 'leeee', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'asas'),
(233, NULL, 5, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'leee'),
(234, NULL, 5, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'this'),
(239, 'cachelleat17@gmail.com', 6, 0, 'djbrjitrtg', '4545', 'dfd', 'sample', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'asas'),
(242, NULL, 5, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'leee'),
(243, NULL, 3, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'empty'),
(244, 'cachelleat17@gmail.com', 6, 0, 'djbrjitrtg', '4545', 'dfd', 'sample', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'asas'),
(245, 'cachelleat17@gmail.com', 6, 0, 'djbrjitrtg', '4545', 'dfd', 'sample', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'asas'),
(246, 'new', 6, 0, 'new', '3434', '3434', 'atty', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', '                     gfgfg    '),
(247, 'new', 6, 0, 'new', '3434', '3434', 'atty', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', '                     gfgfg    '),
(250, 'cachelleat17@gmail.com', 6, 0, 'erererer', '4545', 'sd', 'leeee', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'rtrt'),
(251, 'new', 6, 0, '1112', '4545', 'sd', 'leeee', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'dsfsdfsfsf'),
(258, 'cachelleat17@gmail.com', 6, 0, 'djbrjitrtg', 'new', 'new', 'new', 'new', 'new@gmail.com', 'cachelleat17@gmail.com', 'this'),
(260, 'cachelleat17@gmail.com', 6, 0, 'djbrjitrtg', 'new', 'new', 'new', 'new', 'new@gmail.com', 'cachelleat17@gmail.com', 'this'),
(261, 'cachelleat17@gmail.com', 6, 0, 'djbrjitrtg', 'new', 'new', 'new', 'new', 'new@gmail.com', 'cachelleat17@gmail.com', 'this'),
(263, NULL, 3, NULL, NULL, 'new', 'new', 'new', 'new', 'new@gmail.com', 'cachelleat17@gmail.com', 'this'),
(268, 'cachelleat17@gmail.com', 6, 0, 'djbrjitrtg', '4545', 'sd', 'leeee', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'rtrt'),
(271, 'cachelleat17@gmail.com', 6, 0, 'djbrjitrtg', '4545', 'sd', 'leeee', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'rtrt'),
(275, 'sample', 6, 0, 'sasas', '5656', '3434', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'fdfdgmail.com', 'asas'),
(276, 'sample', 6, 0, '10', '5656', '3434', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(277, 'sample', 6, 0, '10', '5656', '3434', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(278, 'sample', 6, 0, '10', '5656', '3434', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', NULL, NULL),
(280, 'cachelleat17@gmail.com', 7, 0, 'djbrjitrtg', '3434', '3434', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'busy'),
(281, 'new', 7, 0, 'new', 'newn', 'new', 'new', 'new', 'ina_avery@yahoo.com', NULL, NULL),
(282, 'new', 7, 0, 'new', 'newn', 'new', 'new', 'new', 'ina_avery@yahoo.com', 'cachelleat17@gmail.com', '                         wewe'),
(283, 'new', 7, 0, 'new', 'new', 'new', 'new', 'new', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', '                         weewe'),
(284, 'g', 7, 0, 'g', 'g', 'g', 'g', 'g', 'ina_avery@yahoo.com', 'cachelleat17@gmail.com', '                g         '),
(285, 'g', 7, 0, 'g', 'g', 'g', 'g', 'g', 'ina_avery@yahoo.com', 'cachelleat17@gmail.com', '                g         '),
(289, 'neda', 9, 0, 'nedan', 'enda', 'neda', 'neda', 'neda', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'dsds'),
(293, 'cachelleat17@gmail.com', 9, 0, 'djbrjitrtg', 'enda', 'neda', 'neda', 'neda', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'dsds'),
(294, 'new', 9, 0, 'new', 'enda', 'neda', 'neda', 'neda', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'dsds'),
(295, NULL, 9, 0, NULL, 'enda', 'neda', 'neda', 'neda', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'dsds'),
(296, NULL, 5, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'leee'),
(297, NULL, 5, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'leee'),
(298, NULL, 5, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'new', 'nre'),
(299, NULL, 5, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'ria', 'nfdf'),
(300, NULL, 5, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'ria', 'nfdf'),
(301, NULL, 5, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'new', 'nre'),
(302, NULL, 5, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'leee'),
(303, NULL, 9, 0, NULL, 'enda', 'neda', 'neda', 'neda', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'dsds'),
(304, NULL, 9, 0, NULL, 'enda', 'neda', 'neda', 'neda', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'dsds'),
(305, NULL, 9, 0, NULL, 'enda', 'neda', 'neda', 'neda', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'dsds'),
(306, NULL, 9, 0, NULL, 'enda', 'neda', 'neda', 'neda', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'dsds'),
(307, NULL, 1, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'dsd', 'dsdsd'),
(308, NULL, 1, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'dsd', 'dsdsd'),
(309, NULL, 1, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'dsd', 'dsdsd'),
(310, NULL, 1, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'dsd', 'dsdsd'),
(311, NULL, 2, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'sds', 'sdsd'),
(312, NULL, 2, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'sds', 'sdsd'),
(313, NULL, 2, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'sds', 'sdsd'),
(314, NULL, 1, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'dsd', 'dsdsd'),
(315, NULL, 3, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'ss', 'sdsd'),
(316, NULL, 5, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'leee'),
(317, NULL, 5, NULL, NULL, 'dsd', '89634', 'cachelleat17@gmail.com', 'sample', 'cachelleat17@gmail.com', 'cachelleat17@gmail.com', 'leee'),
(321, 'newupdate', 4, 0, NULL, NULL, NULL, 'newupdate', NULL, NULL, NULL, NULL),
(323, 'newupdate', 4, 0, NULL, NULL, NULL, 'newupdate', NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `membership`
--
ALTER TABLE `membership`
  ADD PRIMARY KEY (`membership_id`);

--
-- Indexes for table `sectors`
--
ALTER TABLE `sectors`
  ADD PRIMARY KEY (`sector_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `sector_id` (`sector_id`),
  ADD KEY `membership_id` (`membership_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `membership`
--
ALTER TABLE `membership`
  MODIFY `membership_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sectors`
--
ALTER TABLE `sectors`
  MODIFY `sector_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=324;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
