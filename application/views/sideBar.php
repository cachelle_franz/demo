<?php
if (isset($this->session->userdata['logged_in'])) {
$username = ($this->session->userdata['logged_in']['username']);
$password = ($this->session->userdata['logged_in']['password']);
} else {
header("location: login");
}
?>
<?php
$this->load->view('head');
$this->load->helper('url');
echo base_url();
?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url(); ?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info"> 
          <p><?php echo $username?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <!-- <ul class="sidebar-menu" data-widget="tree">
        <li class="header">NAVIGATION</li>
      
     <li><a href="<?php echo site_url ('login'); ?>"> Add Users</a></li>
      
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Directories</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo site_url ('neda'); ?>"><i class="fa fa-circle-o"></i> NEDA</a></li>
            <li><a href="<?php echo site_url ('header'); ?>"><i class="fa fa-circle-o"></i> NRO's</a></li>
            <li><a href="<?php echo site_url ('sdc'); ?>"><i class="fa fa-circle-o"></i> SDC Members</a></li>
            <li><a href="<?php echo site_url ('rluc'); ?>"><i class="fa fa-circle-o"></i>RLUC Members</a></li>
            <li><a href="<?php echo site_url ('welcome'); ?>"><i class="fa fa-circle-o"></i>RLA's</a></li>
            <li><a href="<?php echo site_url ('lgu'); ?>"><i class="fa fa-circle-o"></i>LGU's</a></li>
            <li><a href="<?php echo site_url ('law'); ?>"><i class="fa fa-circle-o"></i>Lawmakers</a></li>
            <li><a href="<?php echo site_url ('suc'); ?>"><i class="fa fa-circle-o"></i>SUC's</a></li>
          </ul>
        </li>
     
      </ul> -->
    </section>
    <!-- /.sidebar -->
  </aside>