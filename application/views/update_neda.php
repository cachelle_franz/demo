
<?php 
$this->load->view('head');
?>

<!-- <style>
body{
  background-color: black; 
  opacity: .1;
  filter: alpha(opacity=30);
} -->
</style>

<head>
<?php
  $this->load->helper('url');
  ?>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Data Tables</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

</head>
<div id="update"  class= "moreinfo-modal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
    
      <div class="modal-header">
        <h2>Update</h2>
      </div>  

<?php
    if(isset($view) && $view == 2)  {

      foreach ($response as $row){
      ?> 
      
      <div class="modal-body">
            <form method="POST" action="<?php echo base_url()?>index.php/neda/update_data">
                    <div class="col-sm-12">
                        <label>Office</label>
                        <input id="office" name='office' value="<?php echo $row->office;?>" required="medicine_name" class="form-control">
                     </div>
                   
                     <div class="col-sm-12">
                        <label>Name</label>
                        <input id="Region" name='username' value='<?php echo  $row->username;?>' required="medicine_name" class="form-control">
                     </div>

                     <div class="col-sm-12">
                        <label>Position</label>
                        <input id="Region" name="position" value='<?php echo $row->position;?>' required="medicine_name" class="form-control">
                     </div>

                     <div class="col-sm-12">
                        <label>Address</label>
                        <input id="Region" name="address" value='<?php echo  $row->address;?>' required="medicine_name" class="form-control">
                     </div>

                     <div class="col-sm-12">
                        <label>Tel. No.</label>
                        <input id="Region" name="tel_no" value='<?php echo $row->tel_no;?>' required="medicine_name" class="form-control">
                     </div>

                     <div class="col-sm-12">
                        <label>Fax. No</label>
                        <input id="Region" name="fax_no" value='<?php echo $row->fax_no;?>' required="medicine_name" class="form-control">
                     </div>

                     <div class="col-sm-12">
                        <label>Email</label>
                        <input id="Region" name="email" value='<?php echo $row->email;?>' required="medicine_name" class="form-control">
                     </div>
                     
                     <div class="col-sm-12">
                        <label>Website</label>
                        <input id="Region" name="website" value='<?php echo $row->website;?>' required="medicine_name" class="form-control">
                     </div>
                     <div class="col-sm-12">
                        <label>Other Info</label>
                        <input id="Region" name="other" value='<?php echo $row->other_info;?>' required="medicine_name" class="form-control">
                     </div>
                    </div>
                 
                  <div class='modal-footer'>
                    <div class='col-sm-12'>
            
                    <button type='submit' class="btn btn-primary" method="POST" name="update" value='<?php echo $row->user_id;?>'>Update</button>
                    <input type='button' class="btn btn-danger"  name="cancel" value='cancel' onclick="history.back();">
              
              </div>
             
      </div>
      <?php
      }
              }
                ?>
                
</form>

  </div>
    </div>

  </div>
  </div>
