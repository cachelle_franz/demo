<!DOCTYPE html>
<html>
<head>
<?php
$this->load->view('head');
?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php
$this->load->view('sidebar');
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Law Makers</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        
         
          <div class="box">
            <div class="box-header">
          
            </div>

                    
      <button id="add"  class="btn btn-primary" type="button" data-toggle="modal" data-target="#myModal">+ Add Number</button> 

<!-- modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    Modal content
    <div class="modal-content">
    
      <div class="modal-header">
        <h2>Add</h2>
        </div>  
      <div class="modal-body">
<form method="POST">
                    <div class="col-sm-12">
                        <label>District</label>
                     <input id="Region" name="region" required="medicine_name" class='form-control' 
                      >                         </div>
                    <div class="col-sm-12">
                        <label>Name</label>
                         <input id="Contact_no" name="name" required="price" class='form-control' 
                      >
                    </div>
                    <div class="col-sm-12">
                        <label>Position</label>
                         <input id="Contact_no" name="position" required="price" class='form-control' 
                      >
                    </div>
                    <div class="col-sm-12">
                        <label>Address</label>
                         <input id="Contact_no" name="address" required="price" class='form-control' 
                      >
                    </div>
                    <div class="col-sm-12">
                        <label>Te.no</label>
                         <input id="Contact_no" name="tel_no" required="price" class='form-control' 
                      >
                    </div>
                    <div class="col-sm-12">
                        <label>Fax.no</label>
                         <input id="Contact_no" name="fax_no" required="price" class='form-control' 
                      >
                      <div class="col-sm-12">
                        <label>Email</label>
                         <input id="Contact_no" name="email" required="price" class='form-control' type="email">
                    </div>


                       <div class="col-sm-12">
                        <label>Website</label>
                         <input id="Contact_no" name="website" required="price" class='form-control' type="email">
                    </div>

                <div class="col-sm-12">
                        <label>Other details</label>
                         <textarea rows="3" cols="20" id="Contact_no" name="other" required="price" class='form-control' type="email">
                         </textarea>
                    </div>
                    </div>
                  </article>
                  </div>
                 
                  <div class="modal-footer">
                    <div class="col-sm-12" method="POST">
                      <button type="submit" name="submit" class="btn btn-primary" > Submit</button>
                    </div>

      </div>
  </div>
    </div>

  </div>
</form>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                <th>District</th>
                  <th>Name</th>
                  <th>Position</th>
                  <th>Address</th>
                  <th>Tel. No.</th>
                  <th>Fax.no.</th>
                  <th>Email</th>  
                  <th>Website</th>
                  <th>Other Info</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                  $i=1;

                  if($law == 0){ 
                    echo "No entries"; 
                    } 
                  else{
                      foreach($law as $row)
                      {
                          echo "<tr>";
                          echo "<td>".$row->office."</td>";
                          echo "<td>".$row->username."</td>";
                          echo "<td>".$row->position."</td>";
                          echo "<td>".$row->address."</td>";
                          echo "<td>".$row->tel_no."</td>";
                          echo "<td>".$row->fax_no."</td>";
                          echo "<td>".$row->email."</td>";
                          echo "<td>".$row->website."</td>";
                          echo "<td>".$row->other_info."</td>";
                          echo '<td><a href="' . site_url() . '/law/update_data?edit='.$row->user_id.'">EDIT</a>'."  | ".'
                          <a href="' . site_url() . '/law/delete?del='.$row->user_id.'">DELETE</a>
                          </td>';
                          echo "</tr>";
                      $i++;
                      }
                    }
                  ?>
                </tbody>
              
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->

     </div>   
      </div>
      <!-- /.row -->

      
    </section>
    <!-- /.content -->

    
  </div>
  <!-- /.content-wrapper -->

  
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                <p>Execution time 5 seconds</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url(); ?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>assets/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
