<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct()
	{
	//call CodeIgniter's default Constructor
	parent::__construct();
	
	//load database libray manually
    $this->load->database();
    $this->load->library('form_validation');


        
// Load session library
    $this->load->library('session');
	$this->load->helper('security'); 
	//load Model
    $this->load->model('Login_db');
    $this->load->model('Db');
    $this->load->helper('url');
    }
 
    public function index(){
        $this->load->helper('url');
        $this->load->view('login');
    }

    public function user_login_process() {

        $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
        
        if ($this->form_validation->run() == FALSE) {
        if(isset($this->session->userdata['logged_in'])){
        $this->rla();
        }else{
        $this->load->view('login');
        }
        } else {
        $data = array(
        'username' => $this->input->post('username'),
        'password' => $this->input->post('password')
        );
        $result = $this->Login_db->login_user($data);
        if ($result == TRUE) {
        
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $result = $this->Login_db->read_user_information($username,$password);
        if ($result != false) {
        $session_data = array(
        'username' => $result[0]->user_name,
        'password' => $result[0]->password,
        );
        // Add user data in session
        $this->session->set_userdata('logged_in', $session_data);
        $this->rla();
        }
        } else {
        $data = array(
        'error_message' => 'Invalid Username or Password'
        );
        $this->load->view('login', $data);
        }
        }
        }
        

        public function rla()
	{

			
		if (!empty($_POST)) {
			$name = $this->input->post('name');
			$position = $this->input->post('position');
			// Checking if everything is there
			if ( $name && $position ) {
				// Loading model
				$this->load->model('Db');
				$data = array(
					'username' => $name,
					'sector_id' =>4,
					'membership_id' => ' ',
					'position' => $position
				);
	
				//  =Calling model
				$id = $this->Db->insert($data);
	
				// You can do something else here
			}
		}

		$query = $this->Db->getrla();
		$dir['num'] = null;
		if($query){
		 $dir['num'] =  $query;
        }	
        
	
		$this->load->view('rla',$dir);

	}

		
	public function update_data(){  
		$this->load->helper('url');

		$edit = $this->input->get('edit');
		$update = $this->input->get('update');
		
		if(isset($edit)){
		$data['response'] = $this->Db->fetch_single_data($edit);
		$data['view'] = 2;
		
		$this->load->view('update_rla',$data);
		}
		else{

			if($this->input->post('update') != NULL ){
				// POST data
				$edit = $this->input->post('update');
                $postData = $this->input->post();
                //load model
                $this->load->model('Db');

                // Update record
                $this->Db->updateUser($postData,$edit);
                // Redirect page
			
			}
			$this->rla();
		}
		}
		
			public function delete(){
				$del = $this->input->get('del');
				$this->Db->delete($del);
				$this->rla();
			}


			public function logout() {

				// Removing session data
				$sess_array = array(
				'username' => ''
				);
				$this->session->unset_userdata('logged_in', $sess_array);
				$data['message_display'] = 'Successfully Logout';
				$this->load->view('login', $data);
				}
				
	public function fetch(){
		$connect = mysqli_connect("localhost", "root", "", "dir");  
		if(isset($_POST["user_id"]))  
		{  
			 $query = "SELECT * FROM user_info WHERE user_id = '".$_POST["user_id"]."'";  
			 $result = mysqli_query($connect, $query);  
			 $row = mysqli_fetch_array($result);  
			 echo json_encode($row);  
		} 
	
	}

}
