<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_db extends CI_Controller {
	public function __construct()
	{
	//call CodeIgniter's default Constructor
	parent::__construct();
	
	//load database libray manually
    $this->load->database();
    $this->load->library('form_validation');

// Load session library
    $this->load->library('session');
	$this->load->helper('security'); 
	//load Model
    $this->load->model('Login');
    $this->load->helper('url');
    }
 
    public function index(){
        $this->load->helper('url');
        $this->load->view('login');
    }

    public function user_login_process() {

        $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
        
        if ($this->form_validation->run() == FALSE) {
        if(isset($this->session->userdata['logged_in'])){
        $this->load->view('nro');
        }else{
        $this->load->view('login');
        }
        } else {
        $data = array(
        'username' => $this->input->post('username'),
        'password' => $this->input->post('password')
        );
        $result = $this->Login->login_user($data);
        if ($result == TRUE) {
        
        $username = $this->input->post('username');
        $result = $this->Login->read_user_information($username);
        if ($result != false) {
        $session_data = array(
        'username' => $result[0]->user_name,
        'email' => $result[0]->user_email,
        );
        // Add user data in session
        $this->session->set_userdata('logged_in', $session_data);
        $this->load->view('nro');
        }
        } else {
        $data = array(
        'error_message' => 'Invalid Username or Password'
        );
        $this->load->view('login', $data);
        }
        }
        }
        
}
