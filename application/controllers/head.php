<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Head extends CI_Controller {
	public function __construct()
	{
	//call CodeIgniter's default Constructor
	parent::__construct();
	
	//load database libray manually
	$this->load->database();
	
	//load Model
	$this->load->model('Db');
	}
	
	public function index()
	{
        $this->load->helper('url');
		$this->load->view('head');
 
   }
}