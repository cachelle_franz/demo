<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Law extends CI_Controller {

		public function __construct()
		{
		//call CodeIgniter's default Constructor
		parent::__construct();
		
		//load database libray manually
		$this->load->database();
		
		//load Model
		$this->load->model('Db');
		}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{

			
		if (!empty($_POST)) {
			$region = $this->input->post('region');
			$name = $this->input->post('name');
			$position = $this->input->post('position');
			$tel_no = $this->input->post('tel_no');
			$fax_no = $this->input->post('fax_no');
			$email = $this->input->post('email');
			$address = $this->input->post('address');
			$member = $this->input->post('member');
			$website = $this->input->post('website');
			$other = $this->input->post('other');
			// Checking if everything is there
			if ($region && $name && $position && $tel_no && $fax_no && $email && $address  ||$website || $other) {
				// Loading model
				$this->load->model('Db');
				$data = array(
					'username' => $name,
					'sector_id' =>6,
					'membership_id' => ' ',
					'office' => $region,
					'tel_no' => $tel_no,
					'fax_no' => $fax_no,
					'position' => $position,
					'address' => $address,
					'email' => $email,
					'website' => $website,
					'other_info' => $other
				);
	
				//  =Calling model
				$id = $this->Db->insert($data);
	
				// You can do something else here
			}
		}

		$query = $this->Db->getLaw();
		$dir['law'] = null;
		if($query){
		 $dir['law'] =  $query;
        }	
        
	
		$this->load->view('lawmakers',$dir);

	}

		
	public function update_data(){  
		$this->load->helper('url');

		$edit = $this->input->get('edit');
		$update = $this->input->get('update');
		
		if(isset($edit)){
		$data['response'] = $this->Db->fetch_single_data($edit);
		$data['view'] = 2;
		
		$this->load->view('update',$data);
		}
		else{

			if($this->input->post('update') != NULL ){
				// POST data
				$edit = $this->input->post('update');
                $postData = $this->input->post();
                //load model
                $this->load->model('Db');

                // Update record
                $this->Db->updateUser($postData,$edit);
                // Redirect page
			
			}
			$this->index();
		}
		}
		
			public function delete(){
				$del = $this->input->get('del');
				$this->Db->delete($del);
				$this->index();
			}
	public function fetch(){
		$connect = mysqli_connect("localhost", "root", "", "dir");  
		if(isset($_POST["user_id"]))  
		{  
			 $query = "SELECT * FROM user_info WHERE user_id = '".$_POST["user_id"]."'";  
			 $result = mysqli_query($connect, $query);  
			 $row = mysqli_fetch_array($result);  
			 echo json_encode($row);  
		} 
	
	}
}
