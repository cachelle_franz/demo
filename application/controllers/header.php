<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Header extends CI_Controller {
	public function __construct()
	{
	//call CodeIgniter's default Constructor
	parent::__construct();
	
	//load database libray manually
	$this->load->database();
	
	//load Model
	$this->load->model('Db');
	}
	
	public function index()
	{
      
		$this->load->helper('url');
		if (!empty($_POST)) {
			$region = $this->input->post('region');
			$name = $this->input->post('name');
			$position = $this->input->post('position');
			$tel_no = $this->input->post('tel_no');
			$fax_no = $this->input->post('fax_no');
			$email = $this->input->post('email');
			$address = $this->input->post('address');
			$member = $this->input->post('member');
			
			$website = $this->input->post('website');
			$other = $this->input->post('other');
			// Checking if everything is there
			if ($region || $name || $position || $tel_no || $fax_no || $email|| $address || $member ||$website || $other) {
				// Loading model
				$this->load->model('Db');
				$data = array(
					'username' => $name,
					'sector_id' =>1,
					'membership_id' => $member,
					'office' => $region,
					'tel_no' => $tel_no,
					'fax_no' => $fax_no,
					'position' => $position,
					'address' => $address,
					'email' => $email,
					'website' => $website,
					'other_info' => $other
				);
	
				//  =Calling model
				$id = $this->Db->insert($data);
	
				// You can do something else here
			}
		}


		$query = $this->Db->getRgo();
		$dir['num'] = null;
		if($query){
		 $dir['num'] =  $query;
		}
		$query = $this->Db->getRegion();
		$dir['reg'] = null;
		if($query){
		 $dir['reg'] =  $query;
		}	  
		$this->load->view('nro', $dir);
 
   }

	public function update_data(){  
		$this->load->helper('url');

		$edit = $this->input->get('edit');
		$update = $this->input->get('update');
		
		if(isset($edit)){
		$data['response'] = $this->Db->fetch_single_data($edit);
		$data['view'] = 2;
		
		$this->load->view('update_nro',$data);
		}
		else{

			if($this->input->post('update') != NULL ){
				// POST data
				$edit = $this->input->post('update');
                $postData = $this->input->post();
                //load model
                $this->load->model('Db');

                // Update record
                $this->Db->updateUser($postData,$edit);
                // Redirect page
			
			}
			$this->index();
		}
		}
		
		public function delete(){
			$del = $this->input->get('del');
			$this->Db->delete($del);
			$this->index();
		}


	public function savedata()
	{
	//load registration view form
        $this->load->helper('url');
	$this->load->view('nro');
	$this->load->view('sdc');
	
	//Check submit button 


	}

}