<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Db extends CI_Model {

	function saverecords($region,$name,$position,$address,$tel_no,$fax_no,$email)
 {
    $this->load->database();
 $query="insert into users user_info('','$name','','',$region','$tel_no','$fax_no','$position','$email')";
 $this->db->query($query);
 $this->db->_error_message($query); 
 }


 public function insert($data) {
    // Inserting into your table
    $this->db->insert('user_info', $data);
    // Return the id of inserted row
    return $idOfInsertedData = $this->db->insert_id();
}

public function displayrecords()
 {
 $query=$this->db->query("select * from user_info ");
 return $query->result();
 }

 
 function getRgo(){
    $this->db->select("*");
    $this->db->from('user_info');
    $this->db->where('sector_id =1 and membership_id=6');
    $query = $this->db->get();
    return $query->result();
   }

   function getNeda(){
    $this->db->select("*");
    $this->db->from('user_info');
    $this->db->where('sector_id =9  and membership_id=" "');
    $query = $this->db->get();
    return $query->result();
   }

   function getSuc(){
    $this->db->select("*");
    $this->db->from('user_info');
    $this->db->where('sector_id =7 and membership_id=" " ');
    $query = $this->db->get();
    return $query->result();
   }


   function getRegion(){
    $this->db->select("*");
    $this->db->from('user_info');
    $this->db->where('sector_id =1 and membership_id=7');
    $query = $this->db->get();
    return $query->result();
   }

   function getsdc1(){
    $this->db->select("*");
    $this->db->from('user_info');
    $this->db->where('sector_id =2 and membership_id=1');
    $query = $this->db->get();
    return $query->result();
   }
   function getsdc2(){
    $this->db->select("*");
    $this->db->from('user_info');
    $this->db->where('sector_id =2 and membership_id=2');
    $query = $this->db->get();
    return $query->result();
   }
   function getsdc3(){
    $this->db->select("*");
    $this->db->from('user_info');
    $this->db->where('sector_id =2 and membership_id=3');
    $query = $this->db->get();
    return $query->result();
   }

   function getrluc1(){
    $this->db->select("*");
    $this->db->from('user_info');
    $this->db->where('sector_id =3 and membership_id=1');
    $query = $this->db->get();
    return $query->result();
   }
   
   function getrluc2(){
    $this->db->select("*");
    $this->db->from('user_info');
    $this->db->where('sector_id =3 and membership_id=2');
    $query = $this->db->get();
    return $query->result();
   }

   function getrluc3(){
    $this->db->select("*");
    $this->db->from('user_info');
    $this->db->where('sector_id =3 and membership_id=3');
    $query = $this->db->get();
    return $query->result();
   }

   
   function getrla(){
    $this->db->select("*");
    $this->db->from('user_info');
    $this->db->where('sector_id =4 or membership_id=1 ');
    $query = $this->db->get();
    return $query->result();
   }
   function getlgu1(){
    $this->db->select("*");
    $this->db->from('user_info');
    $this->db->where('sector_id =5 and membership_id=2 ');
    $query = $this->db->get();
    return $query->result();
   }
   function getlgu2(){
    $this->db->select("*");
    $this->db->from('user_info');
    $this->db->where('sector_id =5 and membership_id=5 ');
    $query = $this->db->get();
    return $query->result();
   }

   function getlaw(){
    $this->db->select("*");
    $this->db->from('user_info');
    $this->db->where('sector_id =6');
    $query = $this->db->get();
    return $query->result();
   }

   function uplaw($id,$region, $name,$position,$tel_no,$fax_no,$email,$address){
    $this->db->update('user_info');
    $this->db->set('office = $region , username=$name , position =$position tel_no=$tel_no, fax_no=$fax_no, email=$email, address=$address');
    $this->db->where('user_id =$id');
    $query = $this->db->get();
    return $query->result();
   }

   
   function fetch_single_data($idtemp)  
   {   

       $this->db->select("*");       
       $this->db->from("user_info");  
        $this->db->where("user_id", $idtemp);   
        $query = $this->db->get();  
        return $query->result();
        //Select * FROM tbl_user where id = '$id'  
   }  
   
   function updateUser($postData,$edit){   
           
    // $office = trim($postData['office']);
    $name = trim($postData['username']);
    $position = trim($postData['position']);
    // $address = trim($postData['address']);
    // $tel = trim($postData['tel_no']);
    // $fax = trim($postData['fax_no']);
    // $email = trim($postData['email']);    
    // $website = trim($postData['website']);    
    // $other = trim($postData['other']);
    if($name !='' || $email !='' || $office!='' || $position !='' || $address !='' || $tel!='' || $website!='' || $other=''){

        $value=array('username'=>$name,'position'=>$position);
      
        $this->db->where('user_id',$edit);
        $this->db->update('user_info',$value);
        }
    }

        public function delete($id){
            $this->db->where('user_id',$id);
            $this->db->delete('user_info');
    
}
}